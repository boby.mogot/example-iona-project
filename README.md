1.	HibernateCRUDController
Terdapat 4 fungsi utama yaitu Create, Read, Update, dan Delete
Terdapat 3 fungsi tambahan yaitu ReadDetail, DelSert, dan Upsert

a.	Create
Melakukan insert ke database sesuai dengan model yang digunakan. 
Method : POST
Url tambahan : /create
Parameter : - 
Body : Sesuai dengan DTO yang merupakan data parameter yang dapat dikirimkan oleh FE, dengan catatan id model adalah null ( karena merupakan data sequence ).
Contoh : localhost:9110/api/application/create
Contoh Body : {
    "applicationId": null,
    "applicationCode": "BSI002",
    "applicationName": "BSI iBank",
    "url": "www.mandiri.co.id"
}

b.	Read
Menampilkan isi data dari model yang digunakan 
Method : GET
Url tambahan : /readAll
Parameter : filter, orderby, top, skip
•	Filter : digunakan untuk melakukan seleksi pada data yang akan ditampilkan, contohnya filter berdasarkan status.
status =1
•	Orderby : digunakan untuk mengurutkan data yang akan di tampilkan baik descending maupun ascending.
status desc
•	Top : jumlah data yang ingin ditampilkan tiap halamannya (pagination)
10
•	Skip : halaman dari data yang ditampilkan (pagination)
0 = halaman pertama, dan seterusnya
	Body : -
	Contoh : localhost:9110/api/application/readAll?filter=status =1&orderby=status desc&top=10&skip=0

c.	Update
Melakukan perubahan data pada database sesuai dengan model yang digunakan.
Method : PUT
Url tambahan : /update
Parameter: modelID
•	modelID : merupakan representasi dari field id dari model yang digunakan dan valuenya.
Body : Sesuai dengan DTO yang merupakan data parameter yang dapat dikirimkan oleh FE.
Contoh : localhost:9110/api/application/update?modelID=applicationId:1
Contoh body : {
    "applicationId": 1,
    "applicationCode": "BSI002",
    "applicationName": "BSI iBank",
    "url": "www.mandiri.co.id"
}
d.	Delete
Melakukan penghapusan data pada database sesuai dengan model yang digunakan.
Method : DELETE
Url tambahan : /delete
Parameter : modelID
Body : -
Contoh : localhost:9110/api/application/delete?modelID=applicationId:1

e.	ReadDetail
Menampilkan data dengan parameter modelID
Method : GET
Url tambahan : /detail
Parameter : modelID
Body : -
Contoh : localhost:9110/api/application/detail?modelID=applicationId:1
Contoh Body : {
    "applicationId": null,
    "applicationCode": "BSI002",
    "applicationName": "BSI iBank",
    "url": "www.mandiri.co.id"
}


f.	DelSert
Delete + Insert, merupakan penggabungan dari fungsi delete dan insert secara bersamaan
Method : PUT
Url tambahan : /delsert
Parameter : modelID 
Body : Sesuai dengan DTO yang merupakan data parameter yang dapat dikirimkan oleh FE, dengan catatan id model adalah null ( karena merupakan data sequence ).
Contoh : localhost:9110/api/application/detail?modelID=applicationId:1
Contoh body : {
    "applicationId": null,
    "applicationCode": "BSI002",
    "applicationName": "BSI iBank",
    "url": "www.mandiri.co.id"
}

g.	Upsert
Update + Insert, merupakan fungsi untuk update data jika ditemukan data dengan modelID sesuai dengan parameter, dan akan insert ketika tidak ditemukan data yang sesuai.
Untuk propertinya seperti update, hanya berbeda url tambahan saja.
Method : PUT
Url tambahan : /upsert
Parameter: modelID
Body : Sesuai dengan DTO yang merupakan data parameter yang dapat dikirimkan oleh FE.
Contoh : localhost:9110/api/application/update?modelID=applicationId:1
Contoh body : {
    "applicationId": 1,
    "applicationCode": "BSI002",
    "applicationName": "BSI iBank",
    "url": "www.mandiri.co.id"
}

h.	ReadAllAsCSV
Mengcreate file csv dari data readAll, fungsi sepert get readAll, hanya dapat mencetak file csv
Method : GET
Url tambahan : /readAllAsCSV
Parameter : filter, orderby, top, skip
	Body : -
	Contoh : localhost:9110/api/application/readAll?filter=status =1&orderby=status desc&top=10&skip=0

2.	HibernateOptionListController
Digunakan untuk membuat API optionList untuk kebutuhan dropdown/ autocomplete di FE 
Method : GET
Url tambahan : /optionList
Parameter : filter, orderby, top, skip
Body : -
Contoh : localhost:9110/ol/limit/optionList?filter&orderby&top&skip

3.	HibernateViewController
Digunakan untuk membuat view atau menampilkan data yang berasal dari view ( biasanya digunakan untuk data yang berelasi )
Method : GET
Url tambahan : /view
Parameter : filter, orderby, top, skip
Body : -
Contoh : localhost:9110/api/afterlogin/view?filter=&orderby=&top=&skip=&parent_code=

4.	Implement Custom API
Ada 3 jenis custom api, before, on , dan after
- Before merupakan action yang dilakukan sebelum action utama dari fungsi yang dibuat : CustomBeforeReadAll, CustomBeforeInsert, CustomBeforeUpdate
- on merupakan action yang dilakukan dalam action utama fungsi yang dibuat : CustomOnReadAll, CustomOnInsert, CustomOnUpdate
- After merupakan action yang dilakukan setela action utama, misal akan menambahkan kondisi tertentu setelah action utama dilakukan : CustomAfterReadAll,CUstomAfterInsert, CustomAfterUpdate







