/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 110008
 Source Host           : localhost:5432
 Source Catalog        : iona-test
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110008
 File Encoding         : 65001

 Date: 10/08/2020 08:00:08
*/


-- ----------------------------
-- Sequence structure for bucket_approval_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."bucket_approval_id_seq";
CREATE SEQUENCE "public"."bucket_approval_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for position_leave_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."position_leave_id_seq";
CREATE SEQUENCE "public"."position_leave_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for positions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."positions_id_seq";
CREATE SEQUENCE "public"."positions_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for update_date
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."update_date";
CREATE SEQUENCE "public"."update_date" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_leave_request_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_leave_request_id_seq";
CREATE SEQUENCE "public"."user_leave_request_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for bucket_approval
-- ----------------------------
DROP TABLE IF EXISTS "public"."bucket_approval";
CREATE TABLE "public"."bucket_approval" (
  "bucket_approval_id" int8 NOT NULL,
  "user_leave_request_id" int8 NOT NULL,
  "resolver_reason" varchar(255) COLLATE "pg_catalog"."default",
  "resolved_by" varchar(255) COLLATE "pg_catalog"."default",
  "resolved_date" date,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_date" date,
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_date" date,
  "status" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."bucket_approval"."bucket_approval_id" IS 'bucket_approval_id';
COMMENT ON COLUMN "public"."bucket_approval"."user_leave_request_id" IS 'user_leave_request_id';
COMMENT ON COLUMN "public"."bucket_approval"."resolver_reason" IS 'resolver_reason';
COMMENT ON COLUMN "public"."bucket_approval"."resolved_by" IS 'resolved_by';
COMMENT ON COLUMN "public"."bucket_approval"."resolved_date" IS 'resolved_date';
COMMENT ON COLUMN "public"."bucket_approval"."created_by" IS 'created_by';
COMMENT ON COLUMN "public"."bucket_approval"."created_date" IS 'created_date';
COMMENT ON COLUMN "public"."bucket_approval"."updated_by" IS 'updated_by';
COMMENT ON COLUMN "public"."bucket_approval"."updated_date" IS 'updated_date';
COMMENT ON TABLE "public"."bucket_approval" IS 'bucket_approval';

-- ----------------------------
-- Records of bucket_approval
-- ----------------------------
INSERT INTO "public"."bucket_approval" VALUES (6, 1, 'test', 'arif', NULL, NULL, NULL, NULL, NULL, '2');
INSERT INTO "public"."bucket_approval" VALUES (1, 1, 'test', 'arif', NULL, NULL, NULL, NULL, NULL, '2');
INSERT INTO "public"."bucket_approval" VALUES (2, 1, 'test-update', 'arif', NULL, NULL, NULL, NULL, NULL, '2');

-- ----------------------------
-- Table structure for position_leave
-- ----------------------------
DROP TABLE IF EXISTS "public"."position_leave";
CREATE TABLE "public"."position_leave" (
  "position_leave_id" int8 NOT NULL,
  "position_id" int8 NOT NULL,
  "max_leave_day" int2 NOT NULL,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_date" date,
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_date" date
)
;
COMMENT ON COLUMN "public"."position_leave"."position_leave_id" IS 'position_leave_id';
COMMENT ON COLUMN "public"."position_leave"."position_id" IS 'position_id';
COMMENT ON COLUMN "public"."position_leave"."max_leave_day" IS 'max_leave_day';
COMMENT ON COLUMN "public"."position_leave"."created_by" IS 'created_by';
COMMENT ON COLUMN "public"."position_leave"."created_date" IS 'created_date';
COMMENT ON COLUMN "public"."position_leave"."updated_by" IS 'updated_by';
COMMENT ON COLUMN "public"."position_leave"."updated_date" IS 'updated_date';
COMMENT ON TABLE "public"."position_leave" IS 'position_leave';

-- ----------------------------
-- Table structure for positions
-- ----------------------------
DROP TABLE IF EXISTS "public"."positions";
CREATE TABLE "public"."positions" (
  "position_id" int8 NOT NULL,
  "position_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_date" date,
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_date" date
)
;
COMMENT ON COLUMN "public"."positions"."position_id" IS 'position_id';
COMMENT ON COLUMN "public"."positions"."position_name" IS 'position_name';
COMMENT ON COLUMN "public"."positions"."created_by" IS 'created_by';
COMMENT ON COLUMN "public"."positions"."created_date" IS 'created_date';
COMMENT ON COLUMN "public"."positions"."updated_by" IS 'updated_by';
COMMENT ON COLUMN "public"."positions"."updated_date" IS 'updated_date';
COMMENT ON TABLE "public"."positions" IS 'positions';

-- ----------------------------
-- Records of positions
-- ----------------------------
INSERT INTO "public"."positions" VALUES (1, 'pg', 'boby', '2020-07-23', 'boby', '2020-07-23');

-- ----------------------------
-- Table structure for user_leave_request
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_leave_request";
CREATE TABLE "public"."user_leave_request" (
  "user_leave_request_id" int8 NOT NULL,
  "user_id" int8,
  "leave_date_from" date,
  "leave_date_to" date,
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "status" varchar(255) COLLATE "pg_catalog"."default",
  "leave_request_date" date,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_date" date,
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_date" date,
  "leave_day_remaining" int2
)
;

-- ----------------------------
-- Records of user_leave_request
-- ----------------------------
INSERT INTO "public"."user_leave_request" VALUES (1, 1, '2020-07-23', '2020-07-23', 'test', '1', '2020-07-23', 'boby', '2020-07-23', 'boby', '2020-07-23', 1);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "user_id" int8 NOT NULL,
  "position_id" int8 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "address" text COLLATE "pg_catalog"."default",
  "gender" varchar(6) COLLATE "pg_catalog"."default",
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_date" date,
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_date" date,
  "birth_date" date,
  "leave_balance" int2
)
;
COMMENT ON COLUMN "public"."users"."user_id" IS 'user_id';
COMMENT ON COLUMN "public"."users"."position_id" IS 'position_id';
COMMENT ON COLUMN "public"."users"."name" IS 'name';
COMMENT ON TABLE "public"."users" IS 'users';

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (1, 1, 'Boby', 'jalan jkt', 'L', 'Boby', '2020-07-23', 'Boby', '2020-07-23', '2020-07-07', 1);

-- ----------------------------
-- View structure for vm_user_bucket
-- ----------------------------
DROP VIEW IF EXISTS "public"."vm_user_bucket";
CREATE VIEW "public"."vm_user_bucket" AS  SELECT (row_number() OVER (ORDER BY b.bucket_approval_id))::integer AS no,
    b.bucket_approval_id,
    b.resolved_by AS approver_cuti,
    us.name AS pengaju_cuti,
    u.description
   FROM ((bucket_approval b
     JOIN user_leave_request u ON ((u.user_leave_request_id = b.user_leave_request_id)))
     JOIN users us ON ((us.user_id = u.user_id)));

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."bucket_approval_id_seq"', 8, true);
SELECT setval('"public"."position_leave_id_seq"', 3, false);
SELECT setval('"public"."positions_id_seq"', 3, false);
SELECT setval('"public"."update_date"', 3, false);
SELECT setval('"public"."user_leave_request_id_seq"', 3, false);
SELECT setval('"public"."users_id_seq"', 3, false);

-- ----------------------------
-- Indexes structure for table bucket_approval
-- ----------------------------
CREATE UNIQUE INDEX "bucket_approval_pk" ON "public"."bucket_approval" USING btree (
  "bucket_approval_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE INDEX "relationship_4_fk2" ON "public"."bucket_approval" USING btree (
  "user_leave_request_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table bucket_approval
-- ----------------------------
ALTER TABLE "public"."bucket_approval" ADD CONSTRAINT "pk_bucket_approval" PRIMARY KEY ("bucket_approval_id");

-- ----------------------------
-- Indexes structure for table position_leave
-- ----------------------------
CREATE UNIQUE INDEX "position_leave_pk" ON "public"."position_leave" USING btree (
  "position_leave_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table position_leave
-- ----------------------------
ALTER TABLE "public"."position_leave" ADD CONSTRAINT "pk_position_leave" PRIMARY KEY ("position_leave_id");

-- ----------------------------
-- Indexes structure for table positions
-- ----------------------------
CREATE UNIQUE INDEX "position_pk" ON "public"."positions" USING btree (
  "position_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table positions
-- ----------------------------
ALTER TABLE "public"."positions" ADD CONSTRAINT "pk_positions" PRIMARY KEY ("position_id");

-- ----------------------------
-- Primary Key structure for table user_leave_request
-- ----------------------------
ALTER TABLE "public"."user_leave_request" ADD CONSTRAINT "user_leave_request_pkey" PRIMARY KEY ("user_leave_request_id");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "pk_users" PRIMARY KEY ("user_id");

-- ----------------------------
-- Foreign Keys structure for table bucket_approval
-- ----------------------------
ALTER TABLE "public"."bucket_approval" ADD CONSTRAINT "user_leave_request_fk" FOREIGN KEY ("user_leave_request_id") REFERENCES "public"."user_leave_request" ("user_leave_request_id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Keys structure for table user_leave_request
-- ----------------------------
ALTER TABLE "public"."user_leave_request" ADD CONSTRAINT "user_id" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("user_id") ON DELETE RESTRICT ON UPDATE RESTRICT;
