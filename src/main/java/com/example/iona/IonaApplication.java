package com.example.iona;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IonaApplication {

	public static void main(String[] args) {
		SpringApplication.run(IonaApplication.class, args);
	}

}
