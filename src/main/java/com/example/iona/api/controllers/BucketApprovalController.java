package com.example.iona.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.iona.api.dtos.BucketApprovalDTO;
import com.example.iona.models.BucketApproval;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/bucketApproval")
public class BucketApprovalController extends HibernateCRUDController<BucketApproval, BucketApprovalDTO> {
}

