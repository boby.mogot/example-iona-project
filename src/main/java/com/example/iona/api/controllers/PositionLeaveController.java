package com.example.iona.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.iona.api.dtos.PositionLeaveDTO;
import com.example.iona.models.PositionLeave;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/positionsleave")
public class PositionLeaveController extends HibernateCRUDController<PositionLeave, PositionLeaveDTO>{
}
