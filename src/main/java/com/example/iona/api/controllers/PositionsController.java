package com.example.iona.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.iona.api.dtos.PositionsDTO;
import com.example.iona.models.Positions;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/positions")
public class PositionsController extends HibernateCRUDController<Positions, PositionsDTO>{
	
}
