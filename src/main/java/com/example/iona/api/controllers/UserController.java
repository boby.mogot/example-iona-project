package com.example.iona.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.iona.api.dtos.UsersDTO;
import com.example.iona.models.Users;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/users")
public class UserController extends HibernateCRUDController<Users, UsersDTO>{

}
