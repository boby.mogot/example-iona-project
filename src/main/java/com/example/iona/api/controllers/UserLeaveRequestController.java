package com.example.iona.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.iona.api.dtos.UserLeaveRequestDTO;
import com.example.iona.models.UserLeaveRequest;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/leaveRequest")
public class UserLeaveRequestController extends HibernateCRUDController<UserLeaveRequest, UserLeaveRequestDTO> {
	
}
