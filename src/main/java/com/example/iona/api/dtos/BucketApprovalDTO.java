package com.example.iona.api.dtos;

import java.util.Date;

public class BucketApprovalDTO {
	private long bucketApprovalId;
	private String status;
	private String resolverReason;
	private String resolvedBy;
	private Date resolvedDate;	
	private long userLeaveRequestId;
	private String descriptionRequest;
	
	public BucketApprovalDTO() {
		
	}


	public BucketApprovalDTO(long bucketApprovalId, String status, String resolverReason, String resolvedBy,
			Date resolvedDate) {
		super();
		this.bucketApprovalId = bucketApprovalId;
		this.status = status;
		this.resolverReason = resolverReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
	}


	public long getBucketApprovalId() {
		return bucketApprovalId;
	}


	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getResolverReason() {
		return resolverReason;
	}


	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}


	public String getResolvedBy() {
		return resolvedBy;
	}


	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}


	public Date getResolvedDate() {
		return resolvedDate;
	}


	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}


	public long getUserLeaveRequestId() {
		return userLeaveRequestId;
	}


	public void setUserLeaveRequestId(long userLeaveRequestId) {
		this.userLeaveRequestId = userLeaveRequestId;
	}


	public String getDescriptionRequest() {
		return descriptionRequest;
	}


	public void setDescriptionRequest(String descriptionRequest) {
		this.descriptionRequest = descriptionRequest;
	}
		
	
}
