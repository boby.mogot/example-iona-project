package com.example.iona.api.dtos;

import java.util.Date;

public class PositionLeaveDTO {
	private long positionLeaveId;
	private PositionsDTO positions;
	private Short maxLeaveDay;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public PositionLeaveDTO() {
		
	}
	
	public PositionLeaveDTO(long positionLeaveId, PositionsDTO positions, Short maxLeaveDay, String createdBy,
			Date createdDate, String updatedBy, Date updatedDate) {
		super();
		this.positionLeaveId = positionLeaveId;
		this.positions = positions;
		this.maxLeaveDay = maxLeaveDay;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public long getPositionLeaveId() {
		return positionLeaveId;
	}

	public void setPositionLeaveId(long positionLeaveId) {
		this.positionLeaveId = positionLeaveId;
	}

	public PositionsDTO getPositions() {
		return positions;
	}

	public void setPositions(PositionsDTO positions) {
		this.positions = positions;
	}

	public Short getMaxLeaveDay() {
		return maxLeaveDay;
	}

	public void setMaxLeaveDay(Short maxLeaveDay) {
		this.maxLeaveDay = maxLeaveDay;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
}
