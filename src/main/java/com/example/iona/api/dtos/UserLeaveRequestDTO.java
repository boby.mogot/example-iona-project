package com.example.iona.api.dtos;

import java.util.Date;

public class UserLeaveRequestDTO {
	private long userLeaveRequestId;
	private UsersDTO users;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private String status;
	private Date leaveRequestDate;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private Short leaveDayRemaining;
	
	public UserLeaveRequestDTO() {
		
	}
	
	public UserLeaveRequestDTO(long userLeaveRequestId, UsersDTO users, Date leaveDateFrom, Date leaveDateTo,
			String description, String status, Date leaveRequestDate, String createdBy, Date createdDate,
			String updatedBy, Date updatedDate) {
		super();
		this.userLeaveRequestId = userLeaveRequestId;
		this.users = users;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.status = status;
		this.leaveRequestDate = leaveRequestDate;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public Short getLeaveDayRemaining() {
		return leaveDayRemaining;
	}

	public void setLeaveDayRemaining(Short leaveDayRemaining) {
		this.leaveDayRemaining = leaveDayRemaining;
	}

	public long getUserLeaveRequestId() {
		return userLeaveRequestId;
	}

	public void setUserLeaveRequestId(long userLeaveRequestId) {
		this.userLeaveRequestId = userLeaveRequestId;
	}

	public UsersDTO getUsers() {
		return users;
	}

	public void setUsers(UsersDTO users) {
		this.users = users;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLeaveRequestDate() {
		return leaveRequestDate;
	}

	public void setLeaveRequestDate(Date leaveRequestDate) {
		this.leaveRequestDate = leaveRequestDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	
}
