package com.example.iona.api.dtos;

import java.util.Date;

public class UsersDTO {
	private long userId;
	private PositionsDTO positions;
	private String name;
	private Date birthDate;
	private String address;
	private String gender;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updateDate;
	
	public UsersDTO() {
		
	}
	
	public UsersDTO(long userId, PositionsDTO positions, String name, Date birthDate, String address, String gender,
			String createdBy, Date createdDate, String updatedBy, Date updateDate) {
		super();
		this.userId = userId;
		this.positions = positions;
		this.name = name;
		this.birthDate = birthDate;
		this.address = address;
		this.gender = gender;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updateDate = updateDate;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public PositionsDTO getPositions() {
		return positions;
	}

	public void setPositions(PositionsDTO positions) {
		this.positions = positions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
	
}
