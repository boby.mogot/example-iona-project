package com.example.iona.models;
// Generated Jun 9, 2020 9:03:23 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Positions generated by hbm2java
 */
@Entity
@Table(name = "positions", schema = "public")
public class Positions implements java.io.Serializable {

	private long positionId;
	private String positionName;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private Set<Users> userses = new HashSet<Users>(0);
	private Set<PositionLeave> positionLeaves = new HashSet<PositionLeave>(0);

	public Positions() {
	}

	public Positions(long positionId, String positionName) {
		this.positionId = positionId;
		this.positionName = positionName;
	}

	public Positions(long positionId, String positionName, String createdBy, Date createdDate, String updatedBy,
			Date updatedDate, Set<Users> userses, Set<PositionLeave> positionLeaves) {
		this.positionId = positionId;
		this.positionName = positionName;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
		this.userses = userses;
		this.positionLeaves = positionLeaves;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_positions_id_seq")
	@SequenceGenerator(name = "generator_positions_id_seq", sequenceName = "positions_id_seq", schema = "public", allocationSize = 1)
	@Column(name = "position_id", unique = true, nullable = false)
	public long getPositionId() {
		return this.positionId;
	}

	public void setPositionId(long positionId) {
		this.positionId = positionId;
	}

	@Column(name = "position_name", nullable = false, length = 50)
	public String getPositionName() {
		return this.positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	@Column(name = "created_by", length = 50)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", length = 13)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", length = 13)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "positions")
	public Set<Users> getUserses() {
		return this.userses;
	}

	public void setUserses(Set<Users> userses) {
		this.userses = userses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "positions")
	public Set<PositionLeave> getPositionLeaves() {
		return this.positionLeaves;
	}

	public void setPositionLeaves(Set<PositionLeave> positionLeaves) {
		this.positionLeaves = positionLeaves;
	}

}
