package com.example.iona.ols;
import com.io.iona.core.data.annotations.OptionListKey;

public class BucketApprovalOLO {
	@OptionListKey
	private long bucketApprovalId;
	private String resolverReason;
	
	public long getBucketApprovalId() {
		return bucketApprovalId;
	}
	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}
	public String getResolverReason() {
		return resolverReason;
	}
	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}
	
	
}
