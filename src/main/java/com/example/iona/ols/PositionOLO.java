package com.example.iona.ols;

import com.io.iona.core.data.annotations.OptionListKey;

public class PositionOLO {
	@OptionListKey
	private long positionId;
	private String positionName;
	
	public long getPositionId() {
		return positionId;
	}
	public void setPositionId(long positionId) {
		this.positionId = positionId;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	
	
}
