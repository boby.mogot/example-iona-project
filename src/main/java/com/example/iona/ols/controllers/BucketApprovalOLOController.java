package com.example.iona.ols.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.iona.models.BucketApproval;
import com.example.iona.ols.BucketApprovalOLO;
import com.io.iona.springboot.controllers.HibernateOptionListController;

@RestController
@RequestMapping("/ol/bucket")
public class BucketApprovalOLOController extends HibernateOptionListController<BucketApproval, BucketApprovalOLO> {

}
