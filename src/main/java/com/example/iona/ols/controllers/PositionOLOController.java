package com.example.iona.ols.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.iona.models.Positions;
import com.example.iona.ols.PositionOLO;
import com.io.iona.springboot.controllers.HibernateOptionListController;


@RestController
@RequestMapping("/ol/position")
public class PositionOLOController extends HibernateOptionListController<Positions, PositionOLO> {
	
}