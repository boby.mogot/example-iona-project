package com.example.iona.ols.filter;

import com.io.iona.core.data.annotations.OptionListKey;

public class TahunOLO {
	@OptionListKey
	private int keys;
	private int tahun;
	
	public TahunOLO(int keys, int tahun) {
		this.keys = keys;
		this.tahun = tahun;
	}
	public int getKeys() {
		return keys;
	}
	public void setKeys(int keys) {
		this.keys = keys;
	}
	public int getTahun() {
		return tahun;
	}
	public void setTahun(int tahun) {
		this.tahun = tahun;
	}
	
	
	
}
