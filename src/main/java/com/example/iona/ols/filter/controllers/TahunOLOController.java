package com.example.iona.ols.filter.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.iona.ols.filter.TahunOLO;
import com.io.iona.implementations.pagination.DefaultPagingParameter;
import com.io.iona.springboot.actionflows.custom.CustomOnReadAllItems;
import com.io.iona.springboot.controllers.HibernateOptionListController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;

@RestController
@RequestMapping("/ol/level")
public class TahunOLOController extends HibernateOptionListController<TahunOLO, TahunOLO>
implements CustomOnReadAllItems<TahunOLO, TahunOLO>{

	@Override
	public List<TahunOLO> onReadAllItems(HibernateDataUtility arg0, HibernateDataSource<TahunOLO, TahunOLO> arg1,
			DefaultPagingParameter arg2) throws Exception {
		List<TahunOLO> result = new ArrayList<TahunOLO>();

		TahunOLO level1 = new TahunOLO(1, 2000);
		TahunOLO level2 = new TahunOLO(2, 2005);
		TahunOLO level3 = new TahunOLO(3, 2010);
		TahunOLO level4 = new TahunOLO(4, 2015);
		TahunOLO level5 = new TahunOLO(5, 2020);

		result.add(level1);
		result.add(level2);
		result.add(level3);
		result.add(level4);
		result.add(level5);

		return result;
	}

}
