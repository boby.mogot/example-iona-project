package com.example.iona.process.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.iona.api.dtos.BucketApprovalDTO;
import com.example.iona.models.BucketApproval;
import com.example.iona.repositories.UserLeaveRequestRepository;
import com.io.iona.core.enums.ActionFlow;
import com.io.iona.implementations.pagination.DefaultPagingParameter;
import com.io.iona.springboot.actionflows.custom.CustomAfterReadAll;
import com.io.iona.springboot.actionflows.custom.CustomBeforeReadAll;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;

@RestController
@RequestMapping("/api/bucketApprovalFilter")
public class BucketApprovalFilterByIdController extends HibernateCRUDController<BucketApproval, BucketApprovalDTO> 
implements CustomBeforeReadAll<BucketApproval, BucketApprovalDTO>, CustomAfterReadAll<BucketApproval,BucketApprovalDTO>{

	@Autowired
	UserLeaveRequestRepository userLeaveRepo;
	
	@Override
	public void beforeReadAll(HibernateDataUtility dataUtility, HibernateDataSource<BucketApproval, BucketApprovalDTO> dataSource,
			DefaultPagingParameter pagingParameter) throws Exception {
			List<Object> listData =  new ArrayList<Object>();
			listData.add(1);
			listData.add(2);
			listData.add(3);
			pagingParameter.appendFilterWithInClause("bucketApprovalId", listData);
		
	}

	@Override
	public List<BucketApprovalDTO> afterReadAll(HibernateDataUtility dataUtility,
			HibernateDataSource<BucketApproval, BucketApprovalDTO> dataSource, DefaultPagingParameter pagingParameter) throws Exception {
		List<BucketApproval> newBucket = dataSource.getResult(ActionFlow.ON_READ_ALL_ITEMS, List.class);
		List<BucketApprovalDTO> newListBucketDTO = new ArrayList<BucketApprovalDTO>();
		
		for (BucketApproval bucketApproval : newBucket) {
			BucketApprovalDTO newBucketDTO = new BucketApprovalDTO();
			newBucketDTO.setBucketApprovalId(bucketApproval.getBucketApprovalId());
			newBucketDTO.setResolvedBy(bucketApproval.getResolvedBy());
			newBucketDTO.setResolvedDate(bucketApproval.getUpdatedDate());
			newBucketDTO.setDescriptionRequest(userLeaveRepo.getOne(bucketApproval.getUserLeaveRequest().getUserLeaveRequestId()).getDescription());
			newListBucketDTO.add(newBucketDTO);
		}
		
		return newListBucketDTO;
	}

}

