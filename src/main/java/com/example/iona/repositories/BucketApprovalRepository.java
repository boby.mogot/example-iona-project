package com.example.iona.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.iona.models.BucketApproval;


@Repository
public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Long> {

}
