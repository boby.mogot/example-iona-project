package com.example.iona.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.iona.models.PositionLeave;


@Repository 
public interface PositionLeaveRepository extends JpaRepository<PositionLeave,Long>{

}
