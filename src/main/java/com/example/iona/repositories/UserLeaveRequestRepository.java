package com.example.iona.repositories;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.iona.models.UserLeaveRequest;


@Repository
public interface UserLeaveRequestRepository extends JpaRepository<UserLeaveRequest, Long> {
	
	@Modifying
	@Transactional
	@Query(value = "select * from user_leave_request where user_id = :id order by updated_date limit :data offset :number",
	nativeQuery = true)
	ArrayList<UserLeaveRequest> getPaging(@Param(value = "id") long id, @Param(value = "data") int data, @Param(value = "number") int number);
}
