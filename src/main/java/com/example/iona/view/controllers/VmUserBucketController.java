package com.example.iona.view.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.iona.view.dtos.VmUserBucketDTO;
import com.example.iona.views.VmUserBucket;
import com.io.iona.springboot.controllers.HibernateViewController;

@RestController
@RequestMapping("/api/viewuserbucket")
public class VmUserBucketController extends HibernateViewController<VmUserBucket, VmUserBucketDTO>{

}
