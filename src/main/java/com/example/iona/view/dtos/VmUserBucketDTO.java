package com.example.iona.view.dtos;

public class VmUserBucketDTO {
	private int no;
	private long bucketApprovalId;
	private String approverCuti;
	private String pengajuCuti;
	private String description;
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public long getBucketApprovalId() {
		return bucketApprovalId;
	}
	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}
	public String getApproverCuti() {
		return approverCuti;
	}
	public void setApproverCuti(String approverCuti) {
		this.approverCuti = approverCuti;
	}
	public String getPengajuCuti() {
		return pengajuCuti;
	}
	public void setPengajuCuti(String pengajuCuti) {
		this.pengajuCuti = pengajuCuti;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
