package com.example.iona.views;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "vm_user_bucket", schema = "public")
public class VmUserBucket {
	private int no;
	private long bucketApprovalId;
	private String approverCuti;
	private String pengajuCuti;
	private String description;
	
	@Id
	@Column(name = "no", unique = true, nullable = false)
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	
	@Column(name = "bucket_approval_id")
	public long getBucketApprovalId() {
		return bucketApprovalId;
	}
	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}
	@Column(name = "approver_cuti")
	public String getApproverCuti() {
		return approverCuti;
	}
	public void setApproverCuti(String approverCuti) {
		this.approverCuti = approverCuti;
	}
	@Column(name = "pengaju_cuti")
	public String getPengajuCuti() {
		return pengajuCuti;
	}
	public void setPengajuCuti(String pengajuCuti) {
		this.pengajuCuti = pengajuCuti;
	}
	@Column(name = "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
